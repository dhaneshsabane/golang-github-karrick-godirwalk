Source: golang-github-karrick-godirwalk
Section: devel
Priority: optional
Maintainer: Debian Go Packaging Team <team+pkg-go@tracker.debian.org>
Uploaders: Dhanesh B. Sabane <dhanesh95@disroot.org>
Build-Depends: debhelper (>= 11),
               dh-golang,
               golang-any,
               golang-github-pkg-errors-dev
Standards-Version: 4.3.0
Homepage: https://github.com/karrick/godirwalk
Vcs-Browser: https://salsa.debian.org/go-team/packages/golang-github-karrick-godirwalk
Vcs-Git: https://salsa.debian.org/go-team/packages/golang-github-karrick-godirwalk.git
XS-Go-Import-Path: github.com/karrick/godirwalk
Testsuite: autopkgtest-pkg-go

Package: golang-github-karrick-godirwalk-dev
Architecture: all
Depends: ${misc:Depends},
         golang-github-pkg-errors-dev
Description: Fast directory traversal for Golang
 godirwalk is a library for traversing a directory tree on file system.
 .
 This library will normalize the provided top level directory name based
 on the os-specific path separator by calling filepath.Clean on its first
 argument. However it always provides the pathname created by using the
 correct os-specific path separator when invoking the provided callback
 function.
 .
 This library not only provides functions for traversing a file system
 directory tree, but also for obtaining a list of immediate descendants of
 a particular directory, typically much more quickly than using os.ReadDir
 or os.ReadDirnames.
